import numpy as np
import math
import matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import matplotlib.gridspec as gridspec

# NAME = "I_vs_U_normal.csv"
NAME = "I_vs_U_amplified_x10.csv"
SRC_FOLDER = "raw data"
PIC_FOLDER = "pics" 

def read_file_simple(name):
	f = open(name, "r")
	data = f.readlines()[1:]
	I = np.array([])
	U = np.array([])
	for i in data:
		tmp = i.replace("\n", "").split(", ")
		I = np.append(I, float(tmp[0]))
		U = np.append(U, float(tmp[1]))
	return I, U
	
def read_file_err(name):
	f = open(name, "r")
	data = f.readlines()[1:]
	I = np.array([])
	U = np.array([])
	I_err = np.array([])
	U_err = np.array([])
	for i in data:
		tmp = i.replace("\n", "").split(", ")
		I = np.append(I, float(tmp[0]))
		U = np.append(U, float(tmp[1]))
		I_err = np.append(I_err, 1 / (10**len(tmp[0].split(".")[1])) / 2)
		U_err = np.append(U_err, 1 / (10**len(tmp[1].split(".")[1])) / 2)
		print("test", tmp[0], tmp[1], 1 / (10**len(tmp[0].split(".")[1])), 1 / (10**len(tmp[1].split(".")[1])))
		
	return I, U, I_err, U_err
	
def sort_data_simple(I, U):
	a = np.array([I, U])
	tmp = a[:, a[0].argsort()]
	return tmp[0], tmp[1]

def sort_data_err(I, U, I_err, U_err):
	a = np.array([I, U, I_err, U_err])
	tmp = a[:, a[0].argsort()]
	return tmp[0], tmp[1], tmp[2], tmp[3]
	
def fit(I, U):
	def func(x, a, b):
		return a*x + b
	
	I_max = I.max()
	I_min = I.min()
	U_max = U.max()
	U_min = U.min()
	
	p0 = [(I_min - I_max)/(U_max - U_min), 0]
	popt, cov = curve_fit(func, U, I, p0 = p0)
	x = U
	y = func(x, *popt)
	return x, y, popt
	
def draw_simple(U, I, U_fit, I_fit, scale = "linear"):
	U_fit, I_fit, popt = fit(I, U)	
	fig = plt.figure(layout='constrained')
	gs = gridspec.GridSpec(2, 1, figure=fig)
	ax1 = fig.add_subplot(gs[0])
	ax2 = fig.add_subplot(gs[1])
	ax2.sharex(ax1)
	
	ax1.plot(U, I, marker = "o", linestyle = "", color = "b")
	ax1.plot(U_fit, I_fit)
	
	ax2.plot(U, (I - I_fit) / I * 100, marker = ".", color = "purple")
	ax1.set_title("U vs I dependence")
	ax2.set_title("deviations")
	if scale == "log":
		ax1.set_xscale("log")
		ax1.set_yscale("log")
		ax2.set_xscale("log")
		ax2.set_yscale("log")
	
	ax1.grid(True)
	ax2.grid(True)
	
	ax1.set_ylabel("I (mA)")
	# ax1.set_xlabel("U (V)")
	ax2.set_ylabel("delta I (mA)")
	ax2.set_xlabel("U (V)")
	return fig
	
def draw_err(U, I, U_err, I_err, scale = "linear"):
	U_fit, I_fit, popt = fit(I, U)	
	fig = plt.figure(layout='constrained', figsize=(16,12), dpi = 100)
	gs = gridspec.GridSpec(2, 1, figure=fig)
	ax1 = fig.add_subplot(gs[0])
	ax2 = fig.add_subplot(gs[1])
	ax2.sharex(ax1)
	
	ax1.errorbar(U, I, xerr = U_err, yerr = I_err, marker = ".", linestyle = "", color = "b")
	ax1.plot(U_fit, I_fit)
	text = "I [mA] = a*U [V] + b,\na = %.2f, b = %.2f" % (popt[0], popt[1])
	bbox = dict(boxstyle='round', fc='blanchedalmond', ec='orange', alpha=0.5)
	ax1.text(0.95, 0.07, text, fontsize=20, bbox=bbox, 
			transform=ax1.transAxes, horizontalalignment='right')
	
	ax2.plot(U,  (I - I_fit) / I * 100, marker = ".", color = "purple")
	
	# ax2.fill_between(U,  y1 = I_err / I * 100, y2 = -I_err / I * 100, color = "purple")
	
	ax1.set_title("U vs I dependence")
	ax2.set_title("deviations")
	if scale == "log":
		ax1.set_xscale("log")
		ax1.set_yscale("log")
		ax2.set_xscale("log")
		# ax2.set_yscale("log")
		
	ax1.grid(True)
	ax2.grid(True)
	
	ax1.set_ylabel("I (mA)")
	# ax1.set_xlabel("U (V)")
	ax2.set_ylabel("delta I (%)")
	ax2.set_xlabel("U (V)")
	return fig
	
def main():
	# type = "simple"
	type = "err"
	scale = "log"
	# scale = "linear"
	if type == "simple":
		I, U = read_file_simple("%s/%s" % (SRC_FOLDER, NAME))
		I, U = sort_data_simple(I, U)
		fig = draw_simple(U, I, U_fit, I_fit, scale = scale)
	elif type == "err":
		I, U, I_err, U_err = read_file_err("%s/%s" % (SRC_FOLDER, NAME))
		I, U, I_err, U_err = sort_data_err(I, U, I_err, U_err)
		fig = draw_err(U, I, U_err, I_err, scale = scale)
	
	plt.savefig("%s/%s_%s-scale.png" % (PIC_FOLDER, NAME, scale), dpi = 600)
	plt.show()

if __name__ == "__main__":
	main()
